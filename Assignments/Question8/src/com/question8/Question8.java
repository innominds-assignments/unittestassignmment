package com.question8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Question8 {

	List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 4, 5, 7, 71, 83, 9, 45, 43, 78, 89, 90, 78, 56, 65, 80));

	public List<Integer> getPrimeNumbers() {
		List<Integer> primeNumbersList = new ArrayList<Integer>();
		primeNumbersList = list.stream().filter((Question8::filterForPrimeNumber)).collect(Collectors.toList());
		return primeNumbersList;
	}

	public static boolean filterForPrimeNumber(int n) {
		if (n == 1)
			return false;
		else if (n <= 2) {
			return true;
		} else {
			for (int i = 2; i < n; i++) {
				if (n % i == 0) {
					return false;
				}
			}
			return true;
		}
	}
}
