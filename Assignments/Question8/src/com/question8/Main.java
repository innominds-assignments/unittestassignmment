package com.question8;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		Question8 qu = new Question8();
		List<Integer> primeNumbers = qu.getPrimeNumbers();
		primeNumbers.forEach(System.out::println);

	}

}
