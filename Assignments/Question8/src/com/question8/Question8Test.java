package com.question8;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class Question8Test {

	Question8 qu =new Question8();
	
	@Test
	void testGetPrimeNumbers() {
	List<Integer> list = new ArrayList<>();
	list.add(2);
	list.add(5);
	list.add(7);
	list.add(71);
	list.add(83);
	list.add(43);
	list.add(89);
	
	assertEquals(list, qu.getPrimeNumbers());
	}

}
