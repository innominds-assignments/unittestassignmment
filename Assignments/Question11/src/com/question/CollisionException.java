package com.question;

@SuppressWarnings("serial")
public class CollisionException extends Exception {
	
	public CollisionException(String errMsg) {
		super(errMsg);
	}

}
