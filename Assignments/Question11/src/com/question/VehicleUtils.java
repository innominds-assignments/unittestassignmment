package com.question;

public class VehicleUtils {

	public static boolean checkForCollision(String directionV1, String directionV2) {
		boolean flag;
		Vehicle v1 = new Vehicle(); 
		v1.setDirection(directionV1);
		
		Vehicle v2 = new Vehicle(); 
		v2.setDirection(directionV2);
		
		try {
			if(directionV1.equalsIgnoreCase(directionV2)) {
				flag =true;
				System.out.println("Vehicles are moving in Same Direction");
				return flag;
			}else
				throw new CollisionException("Both Vehicles are moving in Same direction...Do something to avoid collision");
		}catch(CollisionException e) {
			System.out.println(e);
			v2.setDirection(directionV1);
			flag = true;
			System.out.println("The Collision is now avoided by redirecting the vehicle v2");

			System.out.println("Direction of Vehicle 1 => " + v1.getDirection());
			System.out.println("Direction of Vehicle 2 => " + v2.getDirection());
			return flag;
		}

	}
}
