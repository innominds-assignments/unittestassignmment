package com.question;

public class Vehicle {

	private String direction;

	public Vehicle() {
		// TODO Auto-generated constructor stub
	}

	public Vehicle(String direction) {
		super();
		this.direction = direction;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Vehicle [direction=" + direction + "]";
	}

}
