package com.question;

import java.util.Scanner;

public class CollisionApplication {
	public static void main(String[] args) {
		try(Scanner sc= new Scanner(System.in)){
			
			System.out.println("Enter the direction for Vehicle 1 (Left or Right)");
			String directionV1 = sc.next();
		
			System.out.println("Enter the direction for Vehicle 2 (Left or Right)");
			String directionV2 =sc.next();
			
			VehicleUtils.checkForCollision(directionV1, directionV2);
		}
	}
}
