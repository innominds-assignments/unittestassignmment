package test;



import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import com.question.VehicleUtils;

class CollisionTest {

	@Test
	void testCheckForCollision() {
			
		String s1 = "left";
		String s2 = "right";
		
		assertTrue(VehicleUtils.checkForCollision(s1, s2));
		
		String s3 = "left";
		assertTrue(VehicleUtils.checkForCollision(s1, s3));
	}

}
