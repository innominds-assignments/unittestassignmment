package com.question10;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Question10 {

	public static void main(String[] args) {
		try(Scanner sc = new Scanner(System.in)){
			System.out.println("Enter the String");
			String input = sc.nextLine();
			
			checkForDuplicates(input);
		}
	}

	public static List<Character> checkForDuplicates(String input) {
		Map<Character, Integer> charMap = new HashMap<>();
		List<Character> duplicatesChar = new ArrayList<Character>(); 
		
		char[] charArray = input.toCharArray();
		for(char c : charArray) {
			if(charMap.containsKey(c))
				charMap.put(c, charMap.get(c)+1);
			else
				charMap.put(c, 1);
		}
		
		for(Map.Entry<Character, Integer> entry : charMap.entrySet())
		{
			if(entry.getValue()>1) {
				System.out.print(entry.getKey() + " ");
				duplicatesChar.add(entry.getKey());
			}
		}
		return duplicatesChar;

}}
