package com.question10;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class Question10Test {
	
	@Test
	void testStringDuplicate() {
		String input = "evening";
		List<Character> duplicatesChar  = Arrays.asList('e', 'n');
		assertEquals(duplicatesChar, Question10.checkForDuplicates(input)); 
		
	}

}
