package com.ade;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class Question2 {

	public static void main(String[] args) throws IOException {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter the text you want to write");
			String text = sc.nextLine();
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			
			byte[] byteArray = text.getBytes();
		
			byteArrayOutputStream.write(byteArray);
			
			FileOutputStream fileOutputStream1 = new FileOutputStream("myFile1.txt");
			FileOutputStream fileOutputStream2 = new FileOutputStream("myFile2.txt");
			
			byteArrayOutputStream.writeTo(fileOutputStream1);
			byteArrayOutputStream.writeTo(fileOutputStream2);
			
			byteArrayOutputStream.close();
			/*String[] allFileNames = { "myfile1.txt", "myfile2.txt", "myfile3.txt" };
			BufferedWriter writer = null;
			for (int i = 0; i < allFileNames.length; i++) {
				System.out.println("Currently writing into " + allFileNames[i]);
				writer = new BufferedWriter(new FileWriter(allFileNames[i]));
				writer.write(text);
				writer.close(); // <- flush the BufferedWriter
				}
*/			}
		}

}
