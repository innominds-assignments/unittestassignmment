package com.question7;

import java.time.LocalDate;
import java.time.Period;

/**
 * Program to prepare a method to accept 2 dates as parameter and return the difference between those 2 dates.
 * 
 * @author vsahu
 */
public class Question7 {
	
	private LocalDate startDate;
	private LocalDate endDate;
	
	
	/**
	 * Method to find Difference using Period
	 */
	public int findDateDiff(LocalDate startDate, LocalDate endDate) {
	
		Period differenceInDates = Period.between(startDate, endDate);
		int years = differenceInDates.getYears(); 
		
		System.out.println(differenceInDates);
		return years;
	}

}
