package com.question7;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class Question7Test {

	@Test
	void testFindDateDiff() {
		LocalDate startDate = LocalDate.parse("2022-09-05");
		LocalDate endDate = LocalDate.parse("2021-09-05");
		
		Question7 qu = new Question7();
		
		System.out.println(qu.findDateDiff(startDate, endDate));
	}

}
