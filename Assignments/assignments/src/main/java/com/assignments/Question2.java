package com.assignments;

import java.util.Scanner;

public class Question2 {

	public static String checkCase(char character) {
		if (character >= 'A' && character <= 'Z')
			return "Capital case";
		else if (character >= 'a' && character <= 'z')
			return "Small Case";
		else
			return "It is other character";
	}

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter the String!!!");
			String input = sc.next();
			String result = "";
			int numOfDigits;
			char character = input.charAt(0);
			if (input.length() == 1 && !Character.isDigit(character)) {
				result = checkCase(character);
				System.out.println(result);
			} else {
				numOfDigits = countNumberOfDigits(input);
				System.out.println(numOfDigits);
			}

		}
	}

	public static int countNumberOfDigits(String input) {
		int count = 0;
		for (int i = 0; i < input.length(); i++) {
			if (Character.isDigit(input.charAt(i)))
				count++;
		}
		return count;

	}
}
