package com.tax;

public class TaxApplication {

	public static void main(String[] args) {
		Employee e1 = new Employee("Test1", 40000);
		Employee e2 = new Employee("Test2", 90000);
		Employee e3 = new Employee("Test3", 160000);

		System.out.println(TaxUtils.calculateTax(e1.getSalary()));
		System.out.println(TaxUtils.calculateTax(e2.getSalary()));
		System.out.println(TaxUtils.calculateTax(e3.getSalary()));

	}

}
