package com.tax;

public class TaxUtils {

	static double additionalAmount;

	public static double calculateTax(double salary) {
		double slab1 = 60000;
		double slab2 = 150000;
		if (salary <= slab1)
			return 0;
		else if (salary > slab1 && salary <= slab2) {
			additionalAmount = calculateAdditionalAmount(salary, slab1);
			return 0.21 * additionalAmount;
		} else
			additionalAmount = calculateAdditionalAmount(salary, slab2);
		return 0.30 * additionalAmount;
	}

	private static double calculateAdditionalAmount(double salary, double slab) {
		return salary - slab;
	}

}
