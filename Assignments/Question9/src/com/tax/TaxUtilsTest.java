package com.tax;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TaxUtilsTest {

	@Test
	void testCalculateTax() {
	
		Employee emp1 = new Employee("Test1", 40000);
		Employee emp2 = new Employee("Test2", 90000);
		Employee emp3 = new Employee("Test3", 160000);
		
		double taxEmp1 = TaxUtils.calculateTax(emp1.getSalary());
		double taxEmp2 = TaxUtils.calculateTax(emp2.getSalary());
		double taxEmp3 = TaxUtils.calculateTax(emp3.getSalary());
		
		assertEquals(0.0, taxEmp1);
		assertEquals(6300.0, taxEmp2);
		assertEquals(3000.0, taxEmp3);
	}

}
