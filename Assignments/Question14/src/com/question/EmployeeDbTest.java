package com.question;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

class EmployeeDbTest {

	EmployeeDb empDb = new EmployeeDb();
	
	@Test
	void testAddEmployee() {
		Employee e1 = new Employee(7, "test", "test@test.com", 'M', 780000);
		assertTrue(empDb .addEmployee(e1));
	}
	
	@Test
	void testAddSameEmployee() {// if Employee already exist then return false
		Employee e1 = new Employee(7, "test", "test@test.com", 'M', 780000);
		empDb.addEmployee(e1);
		assertFalse(empDb.addEmployee(e1)); 
	}

	@Test
	void testDeleteEmployee() {
		Employee e1 = new Employee(6, "Vaibhav", "vaibhav@test.com", 'M', 60000);
		empDb.addEmployee(e1);
		assertTrue(empDb.deleteEmployee(6));
	}

	@Test
	void testShowPaySlip() {
		Employee e1 = new Employee(6, "Vaibhav", "vaibhav@test.com", 'M', 60000);
		empDb.addEmployee(e1);
		assertEquals("60000.0", empDb.showPaySlip(6));
	}

	@Test
	void testListAll() {
		Employee e1 = new Employee(1, "Vaibhav", "vaibhav@test.com", 'M', 60000);
		Employee e2 = new Employee(2, "Kunal", "Kunal@test.com", 'M', 70000);
		Employee e3 = new Employee(3, "Test", "test@test.com", 'M', 80000);
		empDb.addEmployee(e1);
		empDb.addEmployee(e2);
		empDb.addEmployee(e3);
		Employee[] emp = new Employee[] { e1, e2, e3 };
		assertArrayEquals(emp, empDb.listAll());
	}

}
