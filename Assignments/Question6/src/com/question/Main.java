package com.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {
			Question6 ques = new Question6();

			List<Integer> a1 = new ArrayList<>();
			List<Integer> a2 = new ArrayList<>();

			System.out.println("Enter the upper limit : ");
			int n = sc.nextInt();

			a1 = ques.saveEvenNumbers(n);
			System.out.print("prime Numbers => ");
			for (int i : a1) {
				System.out.print(i + ", ");
			}
			System.out.println();

			a2 = ques.printEvenNumbers(a1);
			System.out.print("doubled prime Numbers => ");
			for (int i : a2) {
				System.out.print(i + ", ");
			}

			System.out.println();
			int number = ques.printEvenNumber(n);
			System.out.println(number);

		}

	}

}
