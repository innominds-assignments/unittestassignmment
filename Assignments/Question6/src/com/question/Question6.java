package com.question;

import java.util.ArrayList;
import java.util.List;

/**
 * Develop a java class with a method saveEvenNumbers(int N) using ArrayList to
 * store even numbers from 2 to N, where N is a integer which is passed as a
 * parameter to the method saveEvenNumbers(). The method should return the
 * ArrayList (A1) created. In the same class create a method
 * printEvenNumbers()which iterates through the arrayList A1 in step 1, and It
 * should multiply each number with 2 and display it in format 4,8,12�.2*N. and
 * add these numbers in a new ArrayList (A2). The new ArrayList (A2) created
 * needs to be returned.
 * 
 * Create a method printEvenNumber(int N) parameter is a number N. This method
 * should search the arrayList (A1) for the existence of the number �N� passed.
 * If exists it should return the Number else return zero.
 * 
 * @author Vaibhav Sahu
 * 
 */
public class Question6 {
	
	List<Integer> list = new ArrayList<>();
	
	List<Integer> listA2 = new ArrayList<>();
	

	/**
	 * @param n is the upper limit to which we need to print the even numbers.
	 * @return list ArrayList containing the prime numbers from 2 to n.
	 */
	public List<Integer> saveEvenNumbers(int n) {
		
		for (int i = 2; i <= n; i++) {
			if (i == n && i % 2 == 0) {
				list.add(i);
			} else if (i % 2 == 0) {
				list.add(i);
			}
			i++;
		}
		return list;
	}

	public List<Integer> printEvenNumbers(List<Integer> a1) {
		a1.forEach(i -> {
			listA2.add(i * 2);
		});
		return listA2;
	}

	public int printEvenNumber(int n) {
		if (list.contains(n))
			return n;
		else
			return 0;
	}

}
