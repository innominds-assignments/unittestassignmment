package com.question;

public class Main {
	
	   public static void main(String[] args)
	    {
	      
	       Question4 thread1= new Question4();
	       Question4 thread2 = new Question4();
	       Question4 thread3 = new Question4();
	      
	        // Setting user thread t1 to Daemon
	        thread1.setDaemon(true);
	              
	        // starting first 2 threads
	        thread1.start();
	        thread2.start();
	  
	        // Setting user thread t3 to Daemon
	        thread3.setDaemon(true);
	        thread3.start();        
	    }

}
