package com.question3;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter the text you want to write");
			String text = sc.nextLine();
			
			Question3 ques = new Question3();
			ques.writeIntoFiles(text);
		}
	}
}
