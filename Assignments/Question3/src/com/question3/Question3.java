package com.question3;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Question3 {

	public void writeIntoFiles(String text) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		byte[] byteArray = text.getBytes();

		try {
			byteArrayOutputStream.write(byteArray);
			FileOutputStream fileOutputStream1 = new FileOutputStream("myFile1.txt");
			FileOutputStream fileOutputStream2 = new FileOutputStream("myFile2.txt");

			byteArrayOutputStream.writeTo(fileOutputStream1);
			byteArrayOutputStream.writeTo(fileOutputStream2);

			byteArrayOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();

		}

	}
}
