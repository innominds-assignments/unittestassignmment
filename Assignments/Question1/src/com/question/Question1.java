package com.question;

public class Question1 {
	 private int nonStaticVariable=0;  
	 private static int staticVariable;
	 
	 
	 static {
		 staticVariable=0;
	 }
	 
	 public Question1() {
		 nonStaticVariable++;
		 staticVariable++;
	 }
	 
	 public int getNonStaticVariable() {
		return nonStaticVariable;
	}
	 
	
	public static int getStaticVariable() {
		return staticVariable;
	} 
	
}


