package com.question;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question1Test {

	@Test
	void testNonStatic() {
	
		 Question1 q1 = new Question1();
		 Question1 q2 = new Question1();
		 Question1 q3 = new Question1();
		 assertNotEquals(3, q3.getNonStaticVariable());

	}

	@Test
	void testStatic() {
		Question1 q4 = new Question1(); 
		Question1 q5 = new Question1(); 
		Question1 q6 = new Question1(); 
		
		assertEquals(6, Question1.getStaticVariable());
	}
}
