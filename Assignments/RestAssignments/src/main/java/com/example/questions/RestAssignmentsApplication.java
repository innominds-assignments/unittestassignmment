package com.example.questions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestAssignmentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestAssignmentsApplication.class, args);
	}

}
