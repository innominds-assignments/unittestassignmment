package com.example.questions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Question16 {

	public static Connection createConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/assignment", "root", "root");
	}

	public boolean insertOperation(int id, String name, String email, String password) {
		try {
			Connection con = createConnection();

			String query = " insert into user (userid, name, email, password) values (?, ?, ?, ?)";
							
			PreparedStatement pst = con.prepareStatement(query);
			pst.setInt(1, id);
			pst.setString(2, name);
			pst.setString(3, email);
			pst.setString(4, password);

			int rowsReturned = pst.executeUpdate(query);
			if (rowsReturned > 0)
				return true;

			pst.close();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	public boolean retrieveOperation() {
		int flag = 0;
		try {
			Connection con = createConnection();
			String query = "Select * from user";
			Statement st = con.createStatement();
			ResultSet rst = st.executeQuery(query);

			while (rst.next()) {
				int id = rst.getInt(1);
				String name = rst.getString(2);
				String email = rst.getString(3);
				String password = rst.getString(4);

				System.out.printf("%d, %s, %s, %s", id, name, email, password);
				++flag;

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (flag == 0)
			return false;
		return true;

	}

	public boolean updateOpertation(String email, String pwd) {

		try {
			Connection con = createConnection();
			String query = "UPDATE user SET password=? WHERE email = ?";

			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, pwd);
			st.setString(2, email);

			int rowsUpdated = st.executeUpdate();
			con.close();
			if (rowsUpdated > 0) {
				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return false;

	}

	public boolean deleteOperation(String email) {

		try {
			Connection con = createConnection();
			String query = "DELETE FROM user WHERE email=?";

			PreparedStatement st = con.prepareStatement(query);
			st.setString(1, email);

			int rowsDeleted = st.executeUpdate();

			con.close();
			if (rowsDeleted > 0) {
				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return false;
	}

	public static void main(String[] args) {
		Question16 question16 = new Question16();

		question16.insertOperation(1, "Vaibhav", "vai@test.com", "1234");
		question16.retrieveOperation();
		question16.updateOpertation("vai@test.com", "12345");
		question16.retrieveOperation();
		question16.deleteOperation("vai@test.com");
		
	}

}
