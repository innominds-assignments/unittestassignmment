package com.example.questions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Question15 {

	public static  int getCountOfTablesOrViews(String str) {

		int count = 0;
		try {

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employees", "root", "root");
			Statement st = con.createStatement();
			ResultSet rs = st
					.executeQuery(" SELECT count(*) AS TOTALNUMBEROFTABLES FROM INFORMATION_SCHEMA." + str + " WHERE TABLE_SCHEMA = 'employees'");
			while (rs.next()) {
				count = rs.getInt(1);
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return count;
	}

	public static void main(String args[]) {

		System.out.println("number of tables " + getCountOfTablesOrViews("tables"));
		System.out.println("number of views " + getCountOfTablesOrViews("views"));
	}
}
