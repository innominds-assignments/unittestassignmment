package com.example.questions;

import java.util.ArrayList;
import java.util.List;

public class Question17 {
	public static List<Integer> list = new ArrayList<>();

	public static List<Integer> getPrimeList(int[] arr) {

		for (int i = 0; i < arr.length; i++) {
			boolean isPrime = true;

			for (int j = 2; j < arr[i] / 2 + 1; j++) {
				if (arr[i] % j == 0) {
					isPrime= false;
				}
			}
			if (isPrime && arr[i] != 1) {
				list.add(arr[i]);
			}
		}
		return list;
	}

	public static void main(String[] args) {

		int arr[] = new int[args.length];

		for (int i = 0; i < args.length; i++) {
			arr[i] = Integer.valueOf(args[i]);
		}

		System.out.println((getPrimeList(arr)));
	}
}
