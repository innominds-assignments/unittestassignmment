package com.example.question18;

public class MyClock {

	private int hour;
	private int minute;
	private int second;
	private String period;
	
	MyClock(){
		
	}
	
	public MyClock(int hour, int minute, int second) {
		try {
			if(validateHour(hour))
				this.hour = hour;
			else {
				throw new InvalidTimeException("Invalid value of hour");
			}
			if(validateMinute(minute))
				this.minute = minute;
			else {
				throw new InvalidTimeException("Invalid value of minute");
			}
			if(validateSecond(second))
				this.second = second;
			else {
				throw new InvalidTimeException("Invalid value of second");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
			
	}

	public static boolean validateHour(int hour) {
		if(hour<=12 && hour>=0)
		return true;
		return false;
	}
	public static boolean validateMinute(int minute) {
		if(minute<=59 && minute>=0)
			return true;
		return false;
	}
	public static boolean validateSecond(int second) {
		if(second<=59 && second>=0)
			return true;
		return false;
	}
	
	public static boolean validateStatus(String status) {
		if(status.equalsIgnoreCase("AM")|| status.equalsIgnoreCase("PM"))
			return true;
		return false;
	}
	
	
	public boolean setPeriod(String period) throws InvalidTimeException{ 

		if (validateStatus(period)) {
			this.period = period;
			return true;
		} else {
			System.out.println("wrong input for period");
			return false;
		}

	}

	public boolean setTime(int hour, int minutes, int seconds) throws InvalidTimeException
	{
		
			if (validateHour(hour)) {
				this.hour = hour;
			} else {
				throw new InvalidTimeException("invalid input for hour");
			}
			if (validateMinute(minutes)) {
				this.minute = minutes;
			} else {
				throw new InvalidTimeException("invalid input for minutes");
			}
			if (validateSecond(seconds)) {
				this.second = seconds;
			} else {
				throw new InvalidTimeException("invalid input for seconds");
			}
			
			return true;
		 
	}	
}
