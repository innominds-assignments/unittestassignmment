package com.example.question18;

@SuppressWarnings("serial")
public class InvalidTimeException extends Exception{

	public InvalidTimeException(String errMsg) {
		super(errMsg);
	}
}
