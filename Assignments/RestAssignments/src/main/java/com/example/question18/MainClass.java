package com.example.question18;

import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter hour : ");
			int hr = sc.nextInt();
			System.out.println("Enter minutes : ");
			int min = sc.nextInt();
			System.out.println("Enter seconds : ");
			int sec = sc.nextInt();

			MyClock clock = new MyClock(hr, min, sec);

			System.out.println(clock.setPeriod("am"));
			System.out.println(clock);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
