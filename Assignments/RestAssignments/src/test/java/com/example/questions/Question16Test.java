package com.example.questions;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class Question16Test {

	Question16 question16 = new Question16();

	@Test
	void testInsertOperation() {
		assertTrue(question16.insertOperation(1, "Vaibhav", "vai@gmail.com", "1234"));
	}

	@Test
	void testDeleteOperation() {
		question16.insertOperation(1, "Vaibhav", "vai@gmail.com", "1234");
		assertTrue(question16.deleteOperation("vai@gmail.com"));
	}

	@Test
	void testRetrieveOperation() {
		question16.insertOperation(1, "Vaibhav", "vai@gmail.com", "1234");

		assertTrue(question16.retrieveOperation());
	}

	@Test
	void testUpdateOperation() {
		question16.insertOperation(1, "Vaibhav", "vai@gmail.com", "1234");
		assertTrue(question16.updateOpertation("vai@gmail.com", "12345"));
	}

}
