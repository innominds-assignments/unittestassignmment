package com.example.questions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class Question17Test {

	@Test
	void test() {
		String arg[] = { "1", "2", "3", "4", "5", "7", "8" };

		List<Integer> list = new ArrayList<>();
		list.add(2);
		list.add(3);
		list.add(5);
		list.add(7);
		Question17.main(arg);
		assertEquals(list, Question17.list);
	}

}
