package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.example.questions.Question15;

class Question15Test {

	//Question15 question = new Question15();

	@Test
	void testNoOfTable() {
		assertEquals(8, Question15.getCountOfTablesOrViews("tables"));
	}

	@Test
	void testNoOfViews() {
		assertEquals(2, Question15.getCountOfTablesOrViews("views"));
	}

}
