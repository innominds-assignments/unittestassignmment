package com.example.question18;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyClockTest {
	
	MyClock clock = new MyClock();

	@Test
	void testSetTime() throws InvalidTimeException {
		assertTrue(clock.setTime(6, 55, 55));
	}
	
	@Test
	void testSettingIncorrectTime() {
		assertThatExceptionOfType(InvalidTimeException.class).isThrownBy(()-> clock.setTime(19, 23, 8));
		
	}

}
