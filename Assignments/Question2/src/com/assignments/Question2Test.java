package com.assignments;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class Question2Test {
	
		Question2 ques = new Question2("B1223", 0);
	

	@Test
	void testCheckCase() {
		char character = 'B';
		char smallChar = 'v';
		assertEquals("Capital case", ques.checkCase(character));
		assertEquals("Small Case", ques.checkCase(smallChar));
	}
	
	@Test
	void testCheckSpecialCharacter() {
		char  spclChar = '*';
		assertEquals("It is Special character", ques.checkCase(spclChar));
	}

	@Test
	void testCountNumberOfDigits() {
		String input = "B1223";
		assertEquals(4, ques.countNumberOfDigits(input));
	}

}
