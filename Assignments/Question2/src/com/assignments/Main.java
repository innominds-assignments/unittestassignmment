package com.assignments;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {

			System.out.println("Enter the String!!!");
			String input = sc.next();
			Question2 question = new Question2(input, 0);

			String result = "";
			int numOfDigits;
			char character = input.charAt(0);
			if (input.length() == 1 && Character.isAlphabetic(character)) {
				result = question.checkCase(character);
				System.out.println(result);
			} else {
				numOfDigits = question.countNumberOfDigits(input);
				System.out.println(numOfDigits);
			}

		}
	}

}
