package com.assignments;

public class Question2 {
	
	private String input;
	//private String result;
	private int numOfDigits;
	

	public Question2(String input, int numOfDigits) {
		super();
		this.input = input;
		this.numOfDigits = numOfDigits;
	}

	public String checkCase(char character) {
		if (character >= 'A' && character <= 'Z')
			return "Capital case";
		else if (character >= 'a' && character <= 'z')
			return "Small Case";
		else
			return "It is Special character";
	}

	

	public int countNumberOfDigits(String input) {
		int count = 0;
		for (int i = 0; i < input.length(); i++) {
			if (Character.isDigit(input.charAt(i)))
				count++;
		}
		return count;

	}
}
